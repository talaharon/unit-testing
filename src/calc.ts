
const sum = (...nums: number[]): number => {
    return nums.reduce((a, c) => a + c, 0);
};

const multiply = (multiplier: number, ...nums: number[]) => {
    return nums.map(ele => ele * multiplier);
};



export {sum,multiply};