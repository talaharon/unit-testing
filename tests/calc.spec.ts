import { expect } from "chai";
import {multiply,sum} from "../src/calc";

describe("Calc Module", function() {
    context("Sum", function() {
        it("should exist", function() {
            expect(sum).to.be.a("function");
        });
        it("should return 5", function() {
            expect(sum(2,3)).to.be.equal(5);
        });
        it("should return 20", function() {
            expect(sum(3,4,2,1,10)).to.be.equal(20);
        });
    });

    context("Multiply", function() {
        it("should exist", async function() {
            expect(multiply).to.be.a("function");
        });
        it("should return [9]", async function() {
            expect(multiply(3,3)).to.deep.equal([9]);
        });
        it("should return [12,6,3,30]", async function() {
            expect(multiply(3,4,2,1,10)).to.deep.equal([12,6,3,30]);
        });
    });
});
